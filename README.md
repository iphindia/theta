# Theta Generator

## Developer info

### Design logic

#### csvtotheta

* Each line in csv has to be read into an array. CSV reader itself cannot go back and forth.
* Once an element is consumed, it can be removed from the array.
* There needs to be a lookahead function which can return all the elements under the current element
* The translation table can be built from the csv directly