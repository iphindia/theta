#!/usr/bin/env python3

'''This program reads a csv file and updates the corresponding form on the server'''

import argparse
import csv
import logging
import os
from fulcrum import Fulcrum

APIKEY = os.environ.get('APIKEY', None)
if not APIKEY:
    try:
        import config
        APIKEY = config.apikey
    except ImportError:
        raise ValueError("You need to either define APIKEY environment variable \
or put it in a config.py file with apikey='actualkeyhere'")

def setup_logging():
    """Sets logging up"""
    try:
        loglevel = os.environ['LOGLEVEL']
    except KeyError:
        loglevel = "warning"
    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    logging.basicConfig(level=numeric_level)

def count_elements(formsection):
        """Deep counts elements"""
        count = 0
        for element in formsection:
            count +=1
            if "elements" in element:
                count += count_elements(element["elements"])
        return count

class CtoT:
    """The class that does everything"""
    # pylint: disable=too-many-instance-attributes,too-many-public-methods

    def __init__(self, formid, csvfile):
        """Initialize things"""
        self.script = ""
        self.labels = {}
        self.choices = {}
        self.visibility_rules = {}
        self.elements = []
        self.instructions = []
        self.indent_character = '-'
        self.script_append = ""

        # The order of fields in next three lines is important
        # It is used later to map instruction to corresponding functions
        self.valid_field_types = [
            "Text",
            "Number",
            "SCQ",
            "Section",
            "Classification",
            "Repeatable",
            "RecordLink",
            "YesNoField",
            "Audio",
            "Signature",
            "Date",
            "Photo",
            "Label"
        ]
        self.handler_functions = [
            self.create_text_field,
            self.create_text_field,
            self.create_choice_field,
            self.create_section,
            self.create_classification_field,
            self.create_repeatable,
            self.create_recordlink_field,
            self.create_yes_no_field,
            self.create_audio_field,
            self.create_signature_field,
            self.create_date_field,
            self.create_photo_field,
            self.create_label_field
        ]
        self.server_field_types = [
            "TextField",
            "TextField",
            "ChoiceField",
            "Section",
            "ClassificationField",
            "Repeatable",
            "RecordLinkField",
            "YesNoField",
            "AudioField",
            "SignatureField",
            "DateTimeField",
            "PhotoField",
            "Label"
        ]

        logging.info("Reading CSV")
        # open csvfile and read into instructions
        with open(csvfile, 'r') as csvfd:
            reader = csv.reader(csvfd)
            # Skip header row
            next(reader)
            # Read the rest
            for row in reader:
                self.instructions.append(row)

        # Download existing form from fulcrum
        logging.info("Downloading form %s", formid)
        self.fulcrum = Fulcrum(key=APIKEY)
        self.form = self.fulcrum.forms.find(formid)
        self.formid = formid

        # Other attributes
        self.current_data_name = ""

    def run(self):
        """Run the actual translation"""
        # set indent level to the indent of the first instruction
        # if you do not want to allow variable starting indent,
        # toggle comment status of next two lines
        # indent_level = 0
        indent_level = self.count_indent(self.instructions[0][0])
        start_line = 0
        lines_processed, self.elements = self.process_section(indent_level, start_line)
        logging.info("Processed %s lines", lines_processed)
        self.choices = self.reprocess_choices(self.choices)

    def not_deleting_questions(self):
        """
        Figures out if the number of elements are increasing,
        that is (no question getting deleted)
        """
        print("Count of new elements is", count_elements(self.elements))
        print("Count of old elements is", count_elements(self.form['form']['elements']))
        return bool(not(count_elements(self.elements) < count_elements(self.form['form']['elements'])))

    def process_section(self, indent_level, start_line):
        '''processes a section to return elements inside it'''

        # an empty list for holding the elements of this section
        elements = []

        # start processing from the start line
        current_line = start_line

        while current_line < len(self.instructions):
            instruction = self.instructions[current_line]
            logging.debug(
                "Processing line %s of instructions at indent_level %s",
                current_line,
                indent_level
            )

            # Figure out if we have finished the section
            field_type = instruction[0]
            if not self.correct_indent(field_type, indent_level):
                logging.debug("End of this subsection")
                break
            field_type = self.strip_indent(field_type, indent_level)
            instruction[0] = field_type
            logging.debug("Found a field of type %s", field_type)

            mapping = dict(zip(self.valid_field_types, self.handler_functions))

            if field_type in mapping:
                # Create a skeleton element
                element = self.create_any_field(instruction)
                # Merge specific fields from the corresponding field handler
                lines_processed, field_element = mapping[field_type](
                    instruction,
                    indent_level,
                    current_line
                )
                element = {**element, **field_element}
                current_line += lines_processed
                # Append to elements
                elements.append(element)
                logging.debug("added to elements: %s", element)
            else:
                self.line_error(current_line, "Unknown Field Type %s" % field_type)

            # Finished processing this line
            current_line += 1
        lines_processed = current_line - start_line
        return lines_processed, elements

    def correct_indent(self, field_type, indent_level):
        """Checks if the indent level is correct for being a sub-element"""
        return bool(self.count_indent(field_type) == indent_level)

    @staticmethod
    def strip_indent(word, indent_level):
        """Removes the indent_level number of characters from beginning"""
        return word[indent_level:]

    def count_indent(self, word):
        """Useful to see the indent level of any word"""
        indent_character = self.indent_character
        indent_character_count = 0
        while word[0] == indent_character:
            indent_character_count += 1
            word = word[1:]
        return indent_character_count

    def field_type_valid(self, field_type):
        """Tells you whether the field type could be valid"""
        return bool(field_type in self.valid_field_types)

    @staticmethod
    def is_required(extras):
        '''Tells you if text in required field means required or optional'''
        return bool("required" in extras)

    @staticmethod
    def defaults_previous(extras):
        """Tells if the previous value should be reused"""
        return bool("previous" in extras)

    @staticmethod
    def allows_other(extras):
        """Is other value allowed"""
        return bool("other" in extras)

    @staticmethod
    def get_linked_set(extras):
        """Gets linked set from extras"""
        for extra in extras:
            if extra.startswith("set"):
                return extra.split('=')[1]
        return None

    @staticmethod
    def get_visibility_rules(extras):
        """Gets visibility rules in the elements (used for javascript)"""
        for extra in extras:
            if extra.startswith("vif"):
                return extra[extra.find("(")+1:extra.find(")")]
        return None

    @staticmethod
    def get_conditions(extras, type_of_condition):
        """Extract rules for directly including in fulcrum"""
        rule_object = []
        for extra in extras:
            if extra.startswith(type_of_condition + '='):
                rules_string = extra.split('=')[1]
                rules_string = rules_string.split('|')
                for rule in rules_string:
                    rule_components = rule.split(':')
                    current_rule = {}
                    current_rule['field_key'] = rule_components[0]
                    current_rule['operator'] = rule_components[1]
                    current_rule['value'] = rule_components[2]
                    rule_object.append(current_rule)
        if not rule_object:
            return None
        else:
            return rule_object

    @staticmethod
    def get_conditions_type(extras, type_of_condition):
        """Differentiates between and/or conditions"""
        for extra in extras:
            if extra.startswith(type_of_condition + '='):
                condition_type = extra.split('=')[1]
                return condition_type
        return "all"

    @staticmethod
    def get_title(extras):
        """Gets the title of the member from extras"""
        for extra in extras:
            if extra.startswith("title"):
                return extra.split('=')[1].split(';')
        return None
    
    @staticmethod
    def get_value(extras, key):
        """Gets the value of the key from extras"""
        for extra in extras:
            if extra.startswith(key+"="):
                return extra.split('=')[1]
        return None

    def translate(self, english, kannada, malayalam, hindi, tamil):
        """ensures translation is done"""
        if not kannada.strip():
            kannada = "kannada"
        if not malayalam.strip():
            malayalam = "malayalam"
        if not hindi.strip():
            hindi = "hindi"
        if not tamil.strip():
            tamil = "tamil"

        kannada = self.append_english(kannada, english)
        malayalam = self.append_english(malayalam, english)
        hindi = self.append_english(hindi, english)
        tamil = self.append_english(tamil, english)

        return kannada, malayalam, hindi, tamil

    @staticmethod
    def append_english(word, english):
        """Appends english"""
        return word + " (" + english + ")"

    def convert_instruction_to_dictionary(self, instruction):
        '''converts the array instruction to dictionary with good keys'''
        [field_type, key, data_name, extras, label, kannada, malayalam, hindi, tamil] = instruction
        kannada, malayalam, hindi, tamil = self.translate(label, kannada, malayalam, hindi, tamil)
        return {
            'field_type': field_type,
            'key': key,
            'data_name': data_name,
            'extras': extras,
            'label': label,
            'kannada': kannada,
            'malayalam': malayalam,
            'hindi': hindi,
            'tamil': tamil
        }

    def create_any_field(self, instruction):
        '''creates skeleton field of any type'''
        [field_type, key, data_name, extras, label, kannada, malayalam, hindi, tamil] = instruction
        kannada, malayalam, hindi, tamil = self.translate(label, kannada, malayalam, hindi, tamil)
        extras = extras.split(',')
        logging.debug("data name is %s", data_name)

        # Store labels for later use
        self.labels[data_name] = {
            "kannada": kannada,
            "malayalam": malayalam,
            "english": label,
            "hindi": hindi,
            "tamil": tamil
        }

        # store the data_name for use in choices, etc.
        self.current_data_name = data_name

        field_mapping = dict(zip(self.valid_field_types, self.server_field_types))
        element = {
            'type': field_mapping[field_type],
            'data_name': data_name,
            'key': key,
            'label': label,
            'required': True if self.is_required(extras) else False,
            'disabled': False,
            'hidden': False,
            'default_value': None,
            'description': None
        }
        if self.defaults_previous(extras):
            element['default_previous_value'] = True
        if self.allows_other(extras):
            element['allow_other'] = True
        if "mcq" in extras:
            element['multiple'] = True
        if "create" in extras:
            element['allow_creating_records'] = True
        if "existing" in extras:
            element['allow_existing_records'] = True
        if "noexisting" in extras:
            element['allow_existing_records'] = False
        if "nomultiple" in extras:
            element['allow_multiple_records'] = False
        if "update" in extras:
            element['allow_updating_records'] = True
        if "hidden" in extras:
            element['hidden'] = True
        if self.get_title(extras) is not None:
            element['title_field_keys'] = self.get_title(extras)

        for key in ["min", "max", "min_length", "max_length"]:
            # we need key_without_underscore because NSP doesn't want underscores in csv
            key_without_underscore = key.replace('_', '')
            if self.get_value(extras, key_without_underscore) is not None:
                element[key] = int(float(self.get_value(extras, key_without_underscore)))

        # this is for custom rules that require javascript
        visibility_rules = self.get_visibility_rules(extras)
        if visibility_rules is not None:
            self.visibility_rules[element['data_name']] = visibility_rules

        # this is for rules that are processed by fulcrum
        visible_conditions = self.get_conditions(extras, "visible_conditions")
        if visible_conditions is not None:
            element['visible_conditions_type'] = self.get_conditions_type(extras, "visible_conditions_type")
            element['visible_conditions_behaviour'] = "clear"
            element['visible_conditions'] = visible_conditions
        required_conditions = self.get_conditions(extras, "required_conditions")
        if required_conditions is not None:
            element['required_conditions_type'] = self.get_conditions_type(extras, "required_conditions_type")
            element['required_conditions_type'] = "all"
            element['required_conditions'] = required_conditions
        return element

    @staticmethod
    def create_text_field(instruction, *_args):
        '''creates an object corresponding to a text field'''
        element = {}
        element['numeric'] = False if instruction[0] == "Text" else True
        return 0, element

    @staticmethod
    def create_yes_no_field(_instruction, *_args):
        """Creates a yes no field"""
        element = {}
        element['positive'] = {
            "label": "Yes",
            "value": "yes"
        }
        element['negative'] = {
            "label": "No",
            "value": "no"
        }
        element["neutral"] = {
            "label": "N/A",
            "value": "n/a"
        }
        element["neutral_enabled"] = True
        return 0, element

    @staticmethod
    def create_audio_field(_instruction, *_args):
        """Creates an audio field"""
        element = {}
        return 0, element

    create_signature_field = create_audio_field
    create_date_field = create_audio_field
    create_photo_field = create_audio_field
    create_label_field = create_audio_field

    def create_section(self, _instruction, indent_level, current_line):
        '''Creates a section'''
        element = {}
        lines_processed, element['elements'] = self.process_section(
            indent_level + 1,
            current_line + 1
        )
        return lines_processed, element

    def create_repeatable(self, _instruction, indent_level, current_line):
        """Creates repeatable fields"""
        element = {}
        lines_processed, element['elements'] = self.process_section(
            indent_level + 1,
            current_line + 1
        )
        return lines_processed, element

    def create_classification_field(self, instruction, *_args):
        """Creates a classification field for daily use"""
        element = {}
        element['classification_set_id'] = self.get_linked_set(
            self.convert_instruction_to_dictionary(instruction)['extras'].split(',')
        )
        logging.debug("Creating Classification field")
        return 0, element

    def create_recordlink_field(self, instruction, *_args):
        """Creates a recordlink field for daily use"""
        element = {}
        element['form_id'] = self.get_linked_set(
            self.convert_instruction_to_dictionary(instruction)['extras'].split(',')
        )
        logging.debug("Creating RecordLink field")
        return 0, element

    def create_choice_field(self, instruction, indent_level, current_line):
        '''creates a choice field'''
        element = {}
        choice_set = self.get_linked_set(
            self.convert_instruction_to_dictionary(instruction)['extras'].split(',')
        )
        if choice_set is not None:
            lines_processed = 0
            element['choice_list_id'] = choice_set
        else:
            lines_processed, element['choices'] = self.process_choices(
                indent_level + 1,
                current_line + 1
            )

        return lines_processed, element

    def process_choices(self, indent_level, start_line):
        '''Processes choices'''
        # create a dictionary to save choices
        self.choices[self.current_data_name] = {}
        choices = []
        current_line = start_line
        while current_line < len(self.instructions):
            instruction = self.instructions[current_line]
            field_type = instruction[0]
            if not self.correct_indent(field_type, indent_level):
                logging.debug("End of these choices")
                break
            field_type = self.strip_indent(field_type, indent_level)
            instruction[0] = field_type
            logging.debug("Found a field of type %s", field_type)

            if field_type == "Choice":
                logging.debug("Adding choice")
                choices.append(self.create_choice(instruction))
            else:
                self.line_error(current_line, "Not a choice")
                break
            current_line += 1
        lines_processed = current_line - start_line
        return lines_processed, choices

    def create_choice(self, instruction):
        '''creates choice required for choice fields'''
        instruction = self.convert_instruction_to_dictionary(instruction)
        value = self.label_to_value(instruction['label'])
        self.choices[self.current_data_name][value] = {
            "kannada": instruction['kannada'],
            "malayalam": instruction['malayalam'],
            "english": instruction['label'],
            "hindi": instruction['hindi'],
            "tamil": instruction['tamil']
        }

        return {
            'label': instruction['label'],
            'value': value
        }

    @staticmethod
    def label_to_value(label):
        """Returns lowercase letter stripped of fancy chars"""
        return label.lower()

    @staticmethod
    def reprocess_choices(choices):
        """
Returns choice object for ready use
{
    data_name: {
        label: {
            "kannada": kannada,
            "malayalam": malayalam,
            ...
        },
        label:
    }
}

becomes

{
    data_name: {
        "kannada": [
            {
                "label": kannada,
                "value": label
            },
            ...
        ]
    }
}
"""
        new_choices = {}
        for data_name in choices:
            new_choices[data_name] = {
                "kannada": [],
                "malayalam": [],
                "english": [],
                "hindi": [],
                "tamil": []
            }
            for value in choices[data_name]:
                for language in "kannada,malayalam,english,hindi,tamil".split(','):
                    new_choices[data_name][language].append({
                        "label": choices[data_name][value][language],
                        "value": value
                    })
        return new_choices

    @staticmethod
    def line_error(line_number, error_message=None):
        """Points out where the error is"""
        if error_message is None:
            error_message = "Error!"
        logging.error("%s at line %s", error_message, line_number + 1)

    @staticmethod
    def get_translation_script():
        """Returns the translation script"""
        return """
function translate(e) {{
  var language = CHOICEVALUE($language);
  DATANAMES().forEach(function(dataName) {{
    // Update field labels
    if (labels[dataName]) {{
      SETLABEL(dataName, labels[dataName][language]);
    }} else {{
      SETLABEL(dataName, null);
    }}
    // Update choice values
    if (choices[dataName]) {{
      SETCHOICES(dataName, choices[dataName][language]);
    }} else {{
      SETCHOICES(dataName, null);
    }}
  }});
}}

ON('load-record', translate);
ON('change', 'language', translate);

""".format()

    def sync(self):
        """Syncs the form to server"""
        logging.debug("form being updated with elements %s", self.elements)
        self.form['form']['elements'] = self.elements
        script = "var labels = "
        script += str(self.labels)
        script += ";\n"
        script += "var choices = "
        script += str(self.choices)
        script += ";\n"
        script += self.get_translation_script()
        script += self.script_append
        self.form['form']['script'] = script
        logging.debug(self.form)
        logging.info("Patch applied. Syncing")
        update_message = self.fulcrum.forms.update(self.formid, self.form)
        if "errors" in update_message['form']:
            print("Errors found!")
            print(update_message)
            exit(1)
        else:
            print(update_message)

def setup_argument_parsing():
    """Adds arguments"""
    parser = argparse.ArgumentParser(description='Update fulcrum.')
    parser.add_argument('--force-delete', dest='force_delete', action='store_const',
                        const=True, default=False,
                        help='whether to forcibly delete data?')
    return parser.parse_args()

def show_help(filename, data_object):
    """which file has issue"""
    print("Was processing", filename)
    print("The form on the server has more questions/elements")
    print("If you are sure about deleting data by decreasing questions, run")
    print("./csvtotheta.py --force-delete")
    exit(1)

if __name__ == "__main__":
    setup_logging()
    ARGS = setup_argument_parsing()
    HOUSES = CtoT('75fe885f-1e3b-4cab-8f2b-63358bbd2d75', 'housesform.csv')
    HOUSES.run()
    HOUSES.script_append = """
function zeropadding(n) {
    n = n.toString()
    if (n.length < 2) {
        n = "0" + n
    }
    return n
}

function houseid(e) {
    var location = CHOICEVALUES($study_site_location);
    if (location != undefined) {
        location.forEach(function(part, index, location){
            location[index] = part.substring(0,3).toUpperCase()
        })
        let date = new Date()
        let year = date.getFullYear().toString().substring(2,4)
        location.push(year)
        let month = date.getMonth() + 1
        location.push(zeropadding(month))
        let day = date.getDate()
        location.push(zeropadding(day))
        location = location.join('')
        var visitnumber = $visit_number;
        if (visitnumber != undefined) {
            SETVALUE('house_id', location + zeropadding(visitnumber))
        }
    }
}
ON('load-record', houseid);
ON('change', 'visit_number', houseid);

var roles_allowed_edit = ['Owner', 'Manager'];
ON('edit-record', function (event) {
    if ($signature_dc) {
        if(!ISROLE(roles_allowed_edit)){
            DATANAMES().forEach(function(dataName) {
                SETREADONLY(dataName, true);
            });
        }
    }
    SETREADONLY('members', false);
    SETREADONLY('each_member', false);
});

ON('load-record', function(event) {
  var updateLocationInfo = function() {
    // get the current device location
    var location = CURRENTLOCATION();

    // if there is no location, display a special message
    if (!location) {
      SETLABEL('gps_info', 'No Location Available');
      return;
    }

    // format the display of the location data
    var message = [
      'Accuracy: ' + location.accuracy,
      'Time: ' + new Date(location.timestamp * 1000).toLocaleString()
    ].join('\\n');

    // set the label property of the label on the form
    SETLABEL('gps_info', message);
  };

  // go ahead and update it now...
  updateLocationInfo();

  // ... and every 3 seconds
  SETINTERVAL(updateLocationInfo, 3000);
});

"""
    if HOUSES.not_deleting_questions() or ARGS.force_delete:
        HOUSES.sync()
    else:
        show_help("houses", HOUSES)
    PEOPLE = CtoT('e10507f6-c401-4e92-8a57-1836a0432ecb', 'peopleform.csv')
    PEOPLE.run()
    PEOPLE.script_append = """
function customValidations(e) {
    var age = NUM($age);
    var smoking_start_age = NUM($q53_age_at_first_daily_smokeless_tobacco);
    var alcohol_start_age = NUM($q68_age_at_regular_alcohol);
    if (smoking_start_age > age) {
        INVALID("Age at start of consuming tobacco greater than current age");
    }
    if (alcohol_start_age > age) {
        INVALID("Age at start of drinking alcohol greater than current age");
    }

    var height = NUM($height);
    var weight = NUM($weight);
    var bmi = (weight*100*100)/(height*height)
    if (bmi < 9 || bmi > 36) {
        INVALID("BMI outside range (9-36), please check height and weight");
    }
}
ON('validate-record', customValidations);
"""
    if PEOPLE.not_deleting_questions() or ARGS.force_delete:
        PEOPLE.sync()
    else:
        show_help("people", PEOPLE)